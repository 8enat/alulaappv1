insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (1,'admin','$2a$10$Vw9LyfARmhlnwBeVGnkCOeOyoYRiqk/8z7mdCVnwETA2kk7REFqVG', true, 'nombre', 'ap1', 'ap2', '1961-07-31', 'dni', 'admin@ad.min', 'female-avatar.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (3,'profe','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Natalie', 'Portman', 'Portman', '1981-07-30', '41234567K', 'alula.proyecto2@gmail.com', 'teacher.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (4,'profa','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'María', 'Sánchez', 'López', '1991-01-16', '32432432H', 'alula.proyecto3@gmail.com', 'female-avatar.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (5,'manolito','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Manolito', 'Pies', 'de Plata', '1991-01-16', '32432432H', 'alula.proyecto4@gmail.com', 'user.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (6,'estu','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'estu', 'estuap1', 'estuap2', '2010-01-06', '49225450C', 'alula.proyecto5@gmail.com', 'user.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (7,'estu2','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'estu', 'estuap1', 'estuap2', '2010-01-06', '49229450C', 'alula.proyecto6@gmail.com', 'user.png');

insert into role (id, role) values (1, 'ADMIN');
insert into role (id, role) values (3, 'TEACHER');
insert into role (id, role) values (4, 'STUDENT');

insert into user_role (user_id, role_id) values (1,1);
insert into user_role (user_id, role_id) values (3,3);
insert into user_role (user_id, role_id) values (4,3);
insert into user_role (user_id, role_id) values (5,3);
insert into user_role (user_id, role_id) values (6,4);
insert into user_role (user_id, role_id) values (7,4);

insert into course (id, name) values (1, '1º Primaria');
insert into course (id, name) values (2, '2º Primaria');

insert into subject (id, name, picture) values (1, 'Matemáticas', 'mat.png');
insert into subject (id, name, picture) values (2, 'Lenguaje', 'len.png');
insert into subject (id, name, picture) values (3, 'Inglés', 'eng.png');
insert into subject (id, name, picture) values (4, 'Ciencias', 'cie.png');

insert into classroom (id, name) values (1, '1º Primaria A');
insert into classroom (id, name) values (2, '1º Primaria B');
insert into classroom (id, name) values (3, '2º Primaria A');

insert into user_course (user_id, course_id) values (3,1);
insert into user_course (user_id, course_id) values (4,2);
insert into user_course (user_id, course_id) values (4,1);
insert into user_course (user_id, course_id) values (5,1);
insert into user_course (user_id, course_id) values (5,2);
insert into user_course (user_id, course_id) values (6,1);
insert into user_course (user_id, course_id) values (7,2);

insert into user_classroom (user_id, classroom_id) values (3,1);
insert into user_classroom (user_id, classroom_id) values (3,2);
insert into user_classroom (user_id, classroom_id) values (4,2);
insert into user_classroom (user_id, classroom_id) values (4,3);
insert into user_classroom (user_id, classroom_id) values (5,1);
insert into user_classroom (user_id, classroom_id) values (5,2);
insert into user_classroom (user_id, classroom_id) values (5,3);
insert into user_classroom (user_id, classroom_id) values (6,1);
insert into user_classroom (user_id, classroom_id) values (7,3);

insert into user_subject (user_id, subject_id) values (3, 1);
insert into user_subject (user_id, subject_id) values (3, 2);
insert into user_subject (user_id, subject_id) values (4, 4);
insert into user_subject (user_id, subject_id) values (4, 1);
insert into user_subject (user_id, subject_id) values (5, 1);
insert into user_subject (user_id, subject_id) values (5, 2);
insert into user_subject (user_id, subject_id) values (5, 3);
insert into user_subject (user_id, subject_id) values (5, 4);
insert into user_subject (user_id, subject_id) values (6, 1);
insert into user_subject (user_id, subject_id) values (6, 2);
insert into user_subject (user_id, subject_id) values (7, 3);
insert into user_subject (user_id, subject_id) values (7, 4);

insert into course_classroom(course_id, classroom_id) values (1, 1);
insert into course_classroom(course_id, classroom_id) values (1, 2);
insert into course_classroom(course_id, classroom_id) values (2, 3);

insert into course_subject (course_id, subject_id) values (1, 1);
insert into course_subject (course_id, subject_id) values (1, 2);
insert into course_subject (course_id, subject_id) values (2, 3);
insert into course_subject (course_id, subject_id) values (2, 4);

insert into task (id, name, description, score, type, creation_date, deadline, completed,subject_id) values(1,'Examen de lengua','Examen segunda evaluacion',10,'Examen','2019-05-12','2019-05-12',true,1);
insert into task (id, name, description, score, type, creation_date, deadline, completed,subject_id) values(2,'Examen de mates','Examen segunda evaluacion',10,'Examen','2019-05-12','2019-05-12',true,1);
insert into user_task (user_id,task_id) values (5,1)
insert into user_task (user_id,task_id) values (6,1)
insert into user_task (user_id,task_id) values (6,2)
