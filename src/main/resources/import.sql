--Usuarios

insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (1,'admin','$2a$10$Vw9LyfARmhlnwBeVGnkCOeOyoYRiqk/8z7mdCVnwETA2kk7REFqVG', true, 'Cristina', 'Cifuentes', 'Cuenca', '1981-07-30', '41882567Z', 'admin@ad.min', 'teacher.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (3,'profe','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Natalia', 'Puerto', 'de Indias', '1983-02-27', '41234567K', 'alula.proyecto2@gmail.com', 'teacher.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (4,'profa','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'María', 'Sánchez', 'López', '1991-01-26', '32432432H', 'alula.proyecto3@gmail.com', 'teacher.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (5,'profe3','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Antonio', 'Fernández', 'Resines', '1961-03-16', '32438732H', 'alula.proyecto4@gmail.com', 'teacher2.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (6,'estu1','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Juan', 'Nieve', 'Barbas', '2010-01-06', '49225440C', 'alula.proyecto@gmail.com', 'user.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (7,'estu2','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Héctor', 'Miguel', 'Salamanca', '2010-01-06', '49229450C', 'alula.proyecto6@gmail.com', 'user.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (8,'estu3','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Andrés', 'Iniesta', 'Luján', '2010-01-16', '49229451C', 'alula.proyecto8@gmail.com', 'user.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (9,'estu4','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Roberto', 'Trashorras', 'Gayoso', '2010-02-07', '49229452C', 'alula.proyecto9@gmail.com', 'user.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (10,'estu5','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Sofía', 'Vergara', 'Vergara', '2010-03-05', '49229453C', 'alula.proyecto10@gmail.com', 'female-user.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (11,'estu6','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'María Zahara', 'Gordillo', 'Campos', '2010-09-10', '49229454C', 'alula.proyecto11@gmail.com', 'female-user.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (12,'estu7','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Rosalía', 'Vila', 'Tobella', '2010-09-25', '49229455C', 'alula.proyecto12@gmail.com', 'female-user.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (13,'estu8','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Virginia', 'Díaz', 'Ávila', '2010-08-15', '49229456C', 'alula.proyecto13@gmail.com', 'female-user.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (14,'estu9','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Elisa', 'Sánchez', 'Martín', '2010-12-02', '49229457C', 'alula.proyecto14@gmail.com', 'female-user.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (15,'estu10','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'David', 'Broncano', 'Aguilera', '2010-01-06', '49229458C', 'alula.proyecto15@gmail.com', 'user.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (16,'profe4','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Manuel', 'Pies', 'de Plata', '1979-05-16', '32432432H', 'alula.proyecto16@gmail.com', 'teacher2.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (17,'profe5','$2a$10$m5MZMJZhlu8.sX3KT7ovtuVEmtdt2qW2A.mmSjgTKFXSaZzP1TOvu', TRUE, 'Juan Ignacio', 'Delgado', 'Alemany', '1977-03-29', '324953432H', 'alula.proyecto17@gmail.com', 'teacher2.png');
insert into user (id, username, password, enabled, name, surname1, surname2, birthday, dni, email, picture) values (18,'admin2','$2a$10$Vw9LyfARmhlnwBeVGnkCOeOyoYRiqk/8z7mdCVnwETA2kk7REFqVG', true, 'Ernesto', 'Alterio', 'Bacaicoa', '1970-09-25', '41333567Z', 'admin2@ad.min', 'teacher2.png');

--Roles

insert into role (id, role) values (1, 'ADMIN');
insert into role (id, role) values (3, 'TEACHER');
insert into role (id, role) values (4, 'STUDENT');

--Asignar roles a usuarios

insert into user_role (user_id, role_id) values (1,1);
insert into user_role (user_id, role_id) values (3,3);
insert into user_role (user_id, role_id) values (4,3);
insert into user_role (user_id, role_id) values (5,3);
insert into user_role (user_id, role_id) values (6,4);
insert into user_role (user_id, role_id) values (7,4);
insert into user_role (user_id, role_id) values (8,4);
insert into user_role (user_id, role_id) values (9,4);
insert into user_role (user_id, role_id) values (10,4);
insert into user_role (user_id, role_id) values (11,4);
insert into user_role (user_id, role_id) values (12,4);
insert into user_role (user_id, role_id) values (13,4);
insert into user_role (user_id, role_id) values (14,4);
insert into user_role (user_id, role_id) values (15,4);
insert into user_role (user_id, role_id) values (16,3);
insert into user_role (user_id, role_id) values (17,3);
insert into user_role (user_id, role_id) values (18,1);

--Cursos

insert into course (id, name) values (1, '1º Primaria');
insert into course (id, name) values (2, '2º Primaria');
insert into course (id, name) values (3, '3º Primaria');
insert into course (id, name) values (4, '4º Primaria');

--Asignaturas

insert into subject (id, name, picture) values (1, 'Matemáticas I', 'mat.png');
insert into subject (id, name, picture) values (2, 'Lenguaje I', 'len.png');
insert into subject (id, name, picture) values (3, 'Inglés I', 'eng.png');
insert into subject (id, name, picture) values (4, 'C. Naturales I', 'cie.png');
insert into subject (id, name, picture) values (5, 'C. Sociales I', 'soc.png');
insert into subject (id, name, picture) values (6, 'E. Artística I', 'art.png');
insert into subject (id, name, picture) values (7, 'E. Física I', 'gim.png');
insert into subject (id, name, picture) values (8, 'Valores sociales I', 'val.png');
insert into subject (id, name, picture) values (9, 'Matemáticas II', 'mat.png');
insert into subject (id, name, picture) values (10, 'Lenguaje II', 'len.png');
insert into subject (id, name, picture) values (11, 'Inglés II', 'eng.png');
insert into subject (id, name, picture) values (12, 'C. Naturales II', 'cie.png');
insert into subject (id, name, picture) values (13, 'C. Sociales II', 'soc.png');
insert into subject (id, name, picture) values (14, 'E. Artística II', 'art.png');
insert into subject (id, name, picture) values (15, 'E. Física II', 'gim.png');
insert into subject (id, name, picture) values (16, 'Valores sociales II', 'val.png');
insert into subject (id, name, picture) values (17, 'Matemáticas III', 'mat.png');
insert into subject (id, name, picture) values (18, 'Lenguaje III', 'len.png');
insert into subject (id, name, picture) values (19, 'Inglés III', 'eng.png');
insert into subject (id, name, picture) values (20, 'C. Naturales III', 'cie.png');
insert into subject (id, name, picture) values (21, 'C. Sociales III', 'soc.png');
insert into subject (id, name, picture) values (22, 'E. Artística III', 'art.png');
insert into subject (id, name, picture) values (23, 'E. Física III', 'gim.png');
insert into subject (id, name, picture) values (24, 'Valores sociales III', 'val.png');
insert into subject (id, name, picture) values (25, 'Matemáticas IV', 'mat.png');
insert into subject (id, name, picture) values (26, 'Lenguaje IV', 'len.png');
insert into subject (id, name, picture) values (27, 'Inglés IV', 'eng.png');
insert into subject (id, name, picture) values (28, 'C. Naturales IV', 'cie.png');
insert into subject (id, name, picture) values (29, 'C. Sociales IV', 'soc.png');
insert into subject (id, name, picture) values (30, 'E. Artística IV', 'art.png');
insert into subject (id, name, picture) values (31, 'E. Física IV', 'gim.png');
insert into subject (id, name, picture) values (32, 'Valores sociales IV', 'val.png');

--Clases

insert into classroom (id, name) values (1, '1º Primaria A');
insert into classroom (id, name) values (2, '1º Primaria B');
insert into classroom (id, name) values (3, '2º Primaria A');
insert into classroom (id, name) values (4, '2º Primaria B');
insert into classroom (id, name) values (5, '3º Primaria A');
insert into classroom (id, name) values (6, '3º Primaria B');
insert into classroom (id, name) values (7, '4º Primaria A');
insert into classroom (id, name) values (8, '4º Primaria B');

--Asignar cursos a usuarios

insert into user_course (user_id, course_id) values (3,1);
insert into user_course (user_id, course_id) values (3,2);
insert into user_course (user_id, course_id) values (4,2);
insert into user_course (user_id, course_id) values (4,1);
insert into user_course (user_id, course_id) values (5,1);
insert into user_course (user_id, course_id) values (5,2);
insert into user_course (user_id, course_id) values (6,1);
insert into user_course (user_id, course_id) values (7,1);
insert into user_course (user_id, course_id) values (8,1);
insert into user_course (user_id, course_id) values (9,1);
insert into user_course (user_id, course_id) values (10,1);
insert into user_course (user_id, course_id) values (11,1);
insert into user_course (user_id, course_id) values (12,1);
insert into user_course (user_id, course_id) values (13,1);
insert into user_course (user_id, course_id) values (14,1);
insert into user_course (user_id, course_id) values (15,1);
insert into user_course (user_id, course_id) values (17,1);
--Asignar clases a usuarios

insert into user_classroom (user_id, classroom_id) values (3,1);
insert into user_classroom (user_id, classroom_id) values (3,2);
insert into user_classroom (user_id, classroom_id) values (3,3);
insert into user_classroom (user_id, classroom_id) values (4,2);
insert into user_classroom (user_id, classroom_id) values (4,3);
insert into user_classroom (user_id, classroom_id) values (5,1);
insert into user_classroom (user_id, classroom_id) values (5,2);
insert into user_classroom (user_id, classroom_id) values (5,3);
insert into user_classroom (user_id, classroom_id) values (6,1);
insert into user_classroom (user_id, classroom_id) values (7,1);
insert into user_classroom (user_id, classroom_id) values (8,1);
insert into user_classroom (user_id, classroom_id) values (9,1);
insert into user_classroom (user_id, classroom_id) values (10,1);
insert into user_classroom (user_id, classroom_id) values (11,1);
insert into user_classroom (user_id, classroom_id) values (12,1);
insert into user_classroom (user_id, classroom_id) values (13,1);
insert into user_classroom (user_id, classroom_id) values (14,1);
insert into user_classroom (user_id, classroom_id) values (15,1);
insert into user_classroom (user_id, classroom_id) values (17,1);

--Asignar asignaturas a usuarios

insert into user_subject (user_id, subject_id) values (3, 1);
insert into user_subject (user_id, subject_id) values (3, 8);
insert into user_subject (user_id, subject_id) values (3, 9);
insert into user_subject (user_id, subject_id) values (3, 15);
insert into user_subject (user_id, subject_id) values (4, 4);
insert into user_subject (user_id, subject_id) values (4, 1);
insert into user_subject (user_id, subject_id) values (5, 1);
insert into user_subject (user_id, subject_id) values (5, 2);
insert into user_subject (user_id, subject_id) values (5, 3);
insert into user_subject (user_id, subject_id) values (5, 4);
insert into user_subject (user_id, subject_id) values (6, 1);
insert into user_subject (user_id, subject_id) values (6, 2);
insert into user_subject (user_id, subject_id) values (6, 3);
insert into user_subject (user_id, subject_id) values (6, 4);
insert into user_subject (user_id, subject_id) values (6, 5);
insert into user_subject (user_id, subject_id) values (6, 6);
insert into user_subject (user_id, subject_id) values (6, 7);
insert into user_subject (user_id, subject_id) values (6, 8);
insert into user_subject (user_id, subject_id) values (7, 1);
insert into user_subject (user_id, subject_id) values (7, 2);
insert into user_subject (user_id, subject_id) values (7, 3);
insert into user_subject (user_id, subject_id) values (7, 4);
insert into user_subject (user_id, subject_id) values (7, 5);
insert into user_subject (user_id, subject_id) values (7, 6);
insert into user_subject (user_id, subject_id) values (7, 7);
insert into user_subject (user_id, subject_id) values (7, 8);
insert into user_subject (user_id, subject_id) values (8, 1);
insert into user_subject (user_id, subject_id) values (8, 2);
insert into user_subject (user_id, subject_id) values (8, 3);
insert into user_subject (user_id, subject_id) values (8, 4);
insert into user_subject (user_id, subject_id) values (8, 5);
insert into user_subject (user_id, subject_id) values (8, 6);
insert into user_subject (user_id, subject_id) values (8, 7);
insert into user_subject (user_id, subject_id) values (8, 8);
insert into user_subject (user_id, subject_id) values (9, 1);
insert into user_subject (user_id, subject_id) values (9, 2);
insert into user_subject (user_id, subject_id) values (9, 3);
insert into user_subject (user_id, subject_id) values (9, 4);
insert into user_subject (user_id, subject_id) values (9, 5);
insert into user_subject (user_id, subject_id) values (9, 6);
insert into user_subject (user_id, subject_id) values (9, 7);
insert into user_subject (user_id, subject_id) values (9, 8);
insert into user_subject (user_id, subject_id) values (10, 1);
insert into user_subject (user_id, subject_id) values (10, 2);
insert into user_subject (user_id, subject_id) values (10, 3);
insert into user_subject (user_id, subject_id) values (10, 4);
insert into user_subject (user_id, subject_id) values (10, 5);
insert into user_subject (user_id, subject_id) values (10, 6);
insert into user_subject (user_id, subject_id) values (10, 7);
insert into user_subject (user_id, subject_id) values (10, 8);
insert into user_subject (user_id, subject_id) values (11, 1);
insert into user_subject (user_id, subject_id) values (11, 2);
insert into user_subject (user_id, subject_id) values (11, 3);
insert into user_subject (user_id, subject_id) values (11, 4);
insert into user_subject (user_id, subject_id) values (11, 5);
insert into user_subject (user_id, subject_id) values (11, 6);
insert into user_subject (user_id, subject_id) values (11, 7);
insert into user_subject (user_id, subject_id) values (11, 8);
insert into user_subject (user_id, subject_id) values (12, 1);
insert into user_subject (user_id, subject_id) values (12, 2);
insert into user_subject (user_id, subject_id) values (12, 3);
insert into user_subject (user_id, subject_id) values (12, 4);
insert into user_subject (user_id, subject_id) values (12, 5);
insert into user_subject (user_id, subject_id) values (12, 6);
insert into user_subject (user_id, subject_id) values (12, 7);
insert into user_subject (user_id, subject_id) values (12, 8);
insert into user_subject (user_id, subject_id) values (13, 1);
insert into user_subject (user_id, subject_id) values (13, 2);
insert into user_subject (user_id, subject_id) values (13, 3);
insert into user_subject (user_id, subject_id) values (13, 4);
insert into user_subject (user_id, subject_id) values (13, 5);
insert into user_subject (user_id, subject_id) values (13, 6);
insert into user_subject (user_id, subject_id) values (13, 7);
insert into user_subject (user_id, subject_id) values (13, 8);
insert into user_subject (user_id, subject_id) values (14, 1);
insert into user_subject (user_id, subject_id) values (14, 2);
insert into user_subject (user_id, subject_id) values (14, 3);
insert into user_subject (user_id, subject_id) values (14, 4);
insert into user_subject (user_id, subject_id) values (14, 5);
insert into user_subject (user_id, subject_id) values (14, 6);
insert into user_subject (user_id, subject_id) values (14, 7);
insert into user_subject (user_id, subject_id) values (14, 8);
insert into user_subject (user_id, subject_id) values (15, 1);
insert into user_subject (user_id, subject_id) values (15, 2);
insert into user_subject (user_id, subject_id) values (15, 3);
insert into user_subject (user_id, subject_id) values (15, 4);
insert into user_subject (user_id, subject_id) values (15, 5);
insert into user_subject (user_id, subject_id) values (15, 6);
insert into user_subject (user_id, subject_id) values (7,15);
insert into user_subject (user_id, subject_id) values (15, 8);
insert into user_subject (user_id, subject_id) values (17, 1);
insert into user_subject (user_id, subject_id) values (17, 2);
insert into user_subject (user_id, subject_id) values (17, 3);
insert into user_subject (user_id, subject_id) values (17, 4);
insert into user_subject (user_id, subject_id) values (17, 5);
insert into user_subject (user_id, subject_id) values (17, 6);
insert into user_subject (user_id, subject_id) values (17, 7);
insert into user_subject (user_id, subject_id) values (17, 8);


--Asignar clases a cursos

insert into course_classroom(course_id, classroom_id) values (1, 1);
insert into course_classroom(course_id, classroom_id) values (1, 2);
insert into course_classroom(course_id, classroom_id) values (2, 3);
insert into course_classroom(course_id, classroom_id) values (2, 4);

--Asignar asignaturas a cursos

insert into course_subject (course_id, subject_id) values (1, 1);
insert into course_subject (course_id, subject_id) values (1, 2);
insert into course_subject (course_id, subject_id) values (1, 3);
insert into course_subject (course_id, subject_id) values (1, 4);
insert into course_subject (course_id, subject_id) values (1, 5);
insert into course_subject (course_id, subject_id) values (1, 6);
insert into course_subject (course_id, subject_id) values (1, 7);
insert into course_subject (course_id, subject_id) values (1, 8);
insert into course_subject (course_id, subject_id) values (2, 9);
insert into course_subject (course_id, subject_id) values (2, 10);
insert into course_subject (course_id, subject_id) values (2, 11);
insert into course_subject (course_id, subject_id) values (2, 12);
insert into course_subject (course_id, subject_id) values (2, 13);
insert into course_subject (course_id, subject_id) values (2, 14);
insert into course_subject (course_id, subject_id) values (2, 15);
insert into course_subject (course_id, subject_id) values (2, 16);

--Tareas

insert into task (id, name, description, score, type, creation_date, deadline, completed,subject_id) values(1,'Examen de lengua','Examen segunda evaluacion',10,'Examen','2019-05-12','2019-05-12',true,1);
insert into task (id, name, description, score, type, creation_date, deadline, completed,subject_id) values(2,'Examen de mates','Examen segunda evaluacion',10,'Examen','2019-05-12','2019-05-12',true,1);

--Asignar tareas a usuarios

insert into user_task (user_id,task_id) values (7,1)
insert into user_task (user_id,task_id) values (6,1)
insert into user_task (user_id,task_id) values (6,2)
