package org.centronelson;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class P032ecApplication implements CommandLineRunner {
	
	public static void main(String[] args) {
		SpringApplication.run(P032ecApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		
		System.out.println("Alula ON");
		
	}

}

