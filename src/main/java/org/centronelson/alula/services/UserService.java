package org.centronelson.alula.services;

import java.util.List;
import java.util.Optional;

import org.centronelson.alula.models.entity.Attendance;
import org.centronelson.alula.models.entity.Classroom;
import org.centronelson.alula.models.entity.Course;
import org.centronelson.alula.models.entity.Messagge;
import org.centronelson.alula.models.entity.Role;
import org.centronelson.alula.models.entity.Subject;
import org.centronelson.alula.models.entity.Task;
import org.centronelson.alula.models.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
	public User findUserByUsername(String username);
	public User findUserByEmail(String email);
	public Optional<User> findUserById(Long id);
	public void saveUser(User user);
	public void saveUser2(User user);
	public void deleteUser(Long id);
	public Optional<User> findById(Long id);
	public void deleteCourse(Long id);
	public void deleteClassroom(Long id);
	public List<User> findAllUsers();
	public List<User> findUsersBySubjectId(Long subjectId);
	public List<User> findTeachers();
	public List<User> findStudents();
	public List<User> findAdmins();
	public List<Long> findStudentsIdsBySubjectId(Long subjectId);
	
	public void deleteSubject(Long id);
	public List<Subject> findSubjectsByUserAndCourse(String user,String course);
	public void saveSubject(Subject subject);
	public List<Subject> findAllSubjects();
	public List<Subject> findAllSubjectsByOrderByIdAsc();
	public Subject findSubjectByName(String name);
	public Subject findSubjectById(Long id);
	
	public Classroom findClassroomByName(String name);
	public Classroom findClassroomById(Long id);
	public Course findByClassroomId(Long id);
	public List<Classroom>  findAllClassroom();
	public List<Classroom>  findAllClassroomByOrderByNameAsc();
	public void saveClassroom(Classroom classroom);
	
	public void saveCourse(Course course);
	public List<Course> findAllCourses();
	public Course findCourseByName(String course);
	public Course findCourseById(Long id);
	
	public List<Role> findAllRoles();	
	public List<Role> findTeacherStudentRoles();	
	public Role findRoleByName(String role);
	
	public void saveMessage(Messagge messagge);
	public int findMessagesCountByUserOrigAndUserDest(User user1,User user2);
	public List<Messagge> findMessagesByUserOrigAndUserDest(User user1,User user2);	
	
	public void addAttendance(Attendance attendance);
	public List<Attendance> findAllAttendanceByUserId(Long id);
	
	public void saveTask(Task task);
	}