package org.centronelson.alula.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.centronelson.alula.models.dao.AttendanceRepository;
import org.centronelson.alula.models.dao.ClassroomRepository;
import org.centronelson.alula.models.dao.CourseRepository;
import org.centronelson.alula.models.dao.MessaggeRepository;
import org.centronelson.alula.models.dao.RoleRepository;
import org.centronelson.alula.models.dao.SubjectRepository;
import org.centronelson.alula.models.dao.TaskRepository;
import org.centronelson.alula.models.dao.UserRepository;
import org.centronelson.alula.models.entity.Attendance;
import org.centronelson.alula.models.entity.Classroom;
import org.centronelson.alula.models.entity.Course;
import org.centronelson.alula.models.entity.Messagge;
import org.centronelson.alula.models.entity.Role;
import org.centronelson.alula.models.entity.Subject;
import org.centronelson.alula.models.entity.Task;
import org.centronelson.alula.models.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AttendanceRepository attendanceRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private CourseRepository courseRepository;
	@Autowired
	private SubjectRepository subjectRepository;
	@Autowired
	private ClassroomRepository classroomRepository;
	@Autowired
	private MessaggeRepository messaggeRepository;
	@Autowired
	private TaskRepository taskRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
	
	@Override
	public User findUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public User findUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	
	@Override
	public void saveUser(User user) {
		
		user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEnabled(true);
		userRepository.save(user);
	}
	
	@Override
	public void saveUser2(User user) {
		
        user.setEnabled(true);
		userRepository.save(user);
	}

	@Override
	public Optional<User> findById(Long id) {
		// TODO Auto-generated method stub
		Optional<User> user = userRepository.findById(id);
		return user;
	}
	
	/**
	 * Obtener todos los estudiantes que cursan una asignatura
	 * @return listado de estudiantes
	 */
	@Override
	public List<User> findUsersBySubjectId(Long subjectId) {
		// TODO Auto-generated method stub
		
		List<User> users = findStudents();
		Optional<Subject> subject = subjectRepository.findById(subjectId);
		List<User> usersWithSubject = new ArrayList<User>();
		
		for (User user : users) {
			if (user.getSubjects().contains(subject)) {
				usersWithSubject.add(user);
			}
		}
		return usersWithSubject;
	}
	
	/**
	 * Obtener todos los usuarios con rol de estudiante
	 * @return listado de estudiantes
	 */
	@Override
	public List<User> findStudents(){
		
		List<User> studentsUser = new ArrayList<User>();
		Role role = roleRepository.findByRole("STUDENT");
		List<User> allUsers = userRepository.findAllByOrderBySurname1AscSurname2AscNameAsc();
		for (User user : allUsers) {
			if(user.getRoles().contains(role)) {
				studentsUser.add(user);
			}
		}
		return studentsUser;
	}
	
	@Override
	public List<User> findTeachers() {
		// TODO Auto-generated method stub
		List<User> teachers = new ArrayList<User>();
		Role role = roleRepository.findByRole("TEACHER");
		List<User> allUsers = userRepository.findAllByOrderBySurname1AscSurname2AscNameAsc();
		for (User user : allUsers) {
			if(user.getRoles().contains(role)) {
				teachers.add(user);
			}
		}
		return teachers;
	}
	
	@Override
	public List<User> findAdmins() {
		// TODO Auto-generated method stub
		List<User> admins = new ArrayList<User>();
		Role role = roleRepository.findByRole("ADMIN");
		List<User> allUsers = userRepository.findAllByOrderBySurname1AscSurname2AscNameAsc();
		for (User user : allUsers) {
			if(user.getRoles().contains(role)) {
				admins.add(user);
			}
		}
		return admins;
	}
	
	@Override
	public List<Long> findStudentsIdsBySubjectId(Long subjectId) {
		// TODO Auto-generated method stub
		List<Long> ids = new ArrayList<Long>();
		List<User> users = findStudents();
		Subject subject = new Subject();
		Optional<Subject> subjectOptional = subjectRepository.findById(subjectId);
		if(subjectOptional.isPresent()) {
			subject = subjectOptional.get(); 
		}
		
		List<User> usersWithSubject = new ArrayList<User>();
		
		for (User user : users) {
			if (user.getSubjects().contains(subject)) {
				usersWithSubject.add(user);
			}
		}
		 for(User user : usersWithSubject) {
			 ids.add(user.getId());
		 }
		
		return ids;
	}

	@Override
	public Classroom findClassroomByName(String name) {
		
		return classroomRepository.findByName(name);
	}
	
	@Override
	public Course findByClassroomId(Long classroomId) {
		// TODO Auto-generated method stub
		
		Course myCourse = new Course();
		
		//Buscamos todos los cursos existentes en la BBDD
		
		List<Course> allCourses = courseRepository.findAllByOrderByNameAsc();
		
		/*Dentro de cada curso, comparamos los id de las clases que tiene ese curso con la que recibimos por parámetro
		 * hasta que coincide y así obtenemos el curso que buscamos.
		 */

		for (Course course : allCourses) {
			for(Classroom classr: course.getClassrooms()) {
				if(classr.getId().equals(classroomId)) {
					myCourse = course;
				}
			}
			
		}
		
		return myCourse;
	}
	

	@Override
	public List<User> findAllUsers() {
		
		return userRepository.findAll();
	}

	@Override
	public List<Course> findAllCourses() {
		
		return courseRepository.findAllByOrderByNameAsc();
	}

	@Override
	public List<Role> findAllRoles() {
		// TODO Auto-generated method stub
		return roleRepository.findAll();
	}
	
	@Override
	public List<Role> findTeacherStudentRoles() {
		// TODO Auto-generated method stub
		ArrayList<Role> roles = new ArrayList<Role>();
		
		Role role = roleRepository.findByRole("TEACHER");
		roles.add(role);
		role = roleRepository.findByRole("STUDENT");
		roles.add(role);
		
		return roles;
	}

	@Override
	public List<Subject> findAllSubjects() {
		// TODO Auto-generated method stub
		return subjectRepository.findAllByOrderByNameAsc();
	}

	@Override
	public Role findRoleByName(String role) {
		
		return roleRepository.findByRole(role);
	}

	@Override
	public Course findCourseByName(String course) {
		
		return courseRepository.findByName(course);
	}

	@Override
	public Subject findSubjectByName(String name) {
		
		return subjectRepository.findByName(name);
	}
	@Override
	public List<Classroom> findAllClassroom() {
		// TODO Auto-generated method stub
		return classroomRepository.findAll();
	}
	@Override
	public List<Classroom> findAllClassroomByOrderByNameAsc() {
		
		return classroomRepository.findAllByOrderByNameAsc();
	}

	@SuppressWarnings("unlikely-arg-type")
	public List<Subject> findSubjectsByUserAndCourse(String username, String courseName) {
		
		ArrayList<Subject> subjects = new ArrayList<Subject>();
	
		User user = userRepository.findByUsername(username);
		
		Course course = courseRepository.findByName(courseName);
		
		for (Subject subject : user.getSubjects()) {
			
			if(course.getSubjects().contains(subject)) {
				subjects.add(subject);
			}
		}
		
		return subjects;
	}

	@Override
	public Optional<User> findUserById(Long id) {
		// TODO Auto-generated method stub
		return userRepository.findById(id);
	}

	@Override
	public void addAttendance(Attendance attendance) {
		attendanceRepository.save(attendance);
		
	}

	@Override
	public List<Attendance> findAllAttendanceByUserId(Long id) {
		 List<Attendance>  attAux= attendanceRepository.findAll();
		 
		 List<Attendance>  atts= new ArrayList<Attendance>();
		 for(Attendance a : attAux) {		 
			 if  (a.getUserId()==id) {
				 atts.add(a);
				 }
				 
		 }
		return atts;
	}

	@Override
	public Course findCourseById(Long id) {
		// TODO Auto-generated method stub
		Course course = new Course();
		Optional<Course> optCourse = courseRepository.findById(id);
		course = optCourse.get();
		return course;
	}

	@Override
	public void saveMessage(Messagge messagge) {
		messaggeRepository.save(messagge);
		
	}

	@Override
	public int findMessagesCountByUserOrigAndUserDest(User user1,User user2) {
		int count=messaggeRepository.findByUserOrigAndUserDest(user1, user2).size();
		
		return count;
	}

	@Override
	public List<Messagge> findMessagesByUserOrigAndUserDest(User user1, User user2) {
		messaggeRepository.findByUserOrigAndUserDest(user1, user2);
		return null;
	}
	
	@Override
	public Subject findSubjectById(Long id) {
		
		return subjectRepository.findById(id).get();
	}
	
	/**
	 * Guarda un curso.
	 */
	@Override
	public void saveCourse(Course course) {
		// TODO Auto-generated method stub
		courseRepository.save(course);
	}
	
	/**
	 * Guarda una clase.
	 */
	@Override
	public void saveClassroom(Classroom classroom) {
		// TODO Auto-generated method stub
		classroomRepository.save(classroom);
	}
	
	/**
	 * Guarda una asignatura.
	 */
	@Override
	public void saveSubject(Subject subject) {
		// TODO Auto-generated method stub
		subjectRepository.save(subject);
	}

	@Override
	public Classroom findClassroomById(Long id) {
		// TODO Auto-generated method stub
		return classroomRepository.findById(id).orElse(null);
	}

	@Override
	public void deleteUser(Long id) {
		// TODO Auto-generated method stub
		userRepository.deleteById(id);
	}

	@Override
	public void deleteCourse(Long id) {
		// TODO Auto-generated method stub
		courseRepository.deleteById(id);
	}

	@Override
	public void deleteClassroom(Long id) {
		// TODO Auto-generated method stub
		classroomRepository.deleteById(id);
	}

	@Override
	public void deleteSubject(Long id) {
		// TODO Auto-generated method stub
		subjectRepository.deleteById(id);
	}

	@Override
	public List<Subject> findAllSubjectsByOrderByIdAsc() {
		// TODO Auto-generated method stub
		return subjectRepository.findAllByOrderByIdAsc();
	}

	@Override
	public void saveTask(Task task) {
		taskRepository.save(task);
		
	}

	

	

}