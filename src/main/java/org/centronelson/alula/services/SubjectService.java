package org.centronelson.alula.services;

import java.util.List;

import org.centronelson.alula.models.entity.Subject;
import org.springframework.stereotype.Service;

@Service
public interface SubjectService {

	
	public Subject findSubjectbyId(Long subjectId);
	public Subject findByName(String name);
}
