package org.centronelson.alula.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.centronelson.alula.models.dao.CourseRepository;
import org.centronelson.alula.models.dao.SubjectRepository;
import org.centronelson.alula.models.dao.UserRepository;
import org.centronelson.alula.models.entity.Course;
import org.centronelson.alula.models.entity.Subject;
import org.centronelson.alula.models.entity.User;
import org.springframework.beans.factory.annotation.Autowired;

public class SubjectServiceImpl implements SubjectService{
	@Autowired
	private SubjectRepository subjectRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CourseRepository courseRepository;
	
	/**
	 * Método para obtener una asignatura
	 * @param subjectId
	 * @return
	 */
	public Subject findSubjectbyId(Long subjectId) {
		
		Subject subject = new Subject();
		Optional<Subject> subjectOptional = subjectRepository.findById(subjectId);
		if(subjectOptional.isPresent()) {
			subject = subjectOptional.get(); 
		}
		return subject;
	}
	
	/**
	 * Método que obtiene las asignaturas de un usuario y su curso
	 * @param username
	 * @param classroomName
	 * @param courseName
	 * @return
	 */
	@SuppressWarnings("unlikely-arg-type")
	public List<Subject> findSubjectsByUserAndCourse(String username, String courseName) {
		
		ArrayList<Subject> subjects = new ArrayList<Subject>();
	
		User user = userRepository.findByUsername(username);
		
		Course course = courseRepository.findByName(courseName);
		
		for (Subject subject : user.getSubjects()) {
			
			if(course.getSubjects().contains(subject)) {
				subjects.add(subject);
			}
		}
		
		return subjects;
	}

	@Override
	public Subject findByName(String name) {
		
		return subjectRepository.findByName(name);
	}

}
