package org.centronelson.alula.services;

/**
 * Interfaz del servicio email
 * @author Rubén
 *
 */

public interface MailService {
	
	public void sendMail(String from, String to, String subject, String text);
	
}
