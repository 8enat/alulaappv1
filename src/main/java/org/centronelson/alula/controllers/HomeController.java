package org.centronelson.alula.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RandomStringUtils;
import org.centronelson.alula.models.entity.Classroom;
import org.centronelson.alula.models.entity.Course;
import org.centronelson.alula.models.entity.Role;
import org.centronelson.alula.models.entity.Subject;
import org.centronelson.alula.models.entity.User;
import org.centronelson.alula.services.MailService;
import org.centronelson.alula.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private MailService mailService;
	
	@Autowired
    private BCryptPasswordEncoder passwordEncoder;
	
	/**
	 * Obtener el usuario actual
	 * @param principal
	 * @return
	 */
	private User currentUser(Principal principal) {
		return userService.findUserByUsername(principal.getName());
	}
	
	/**
	 * Acceso
	 * @param model
	 * @return formulario de login
	 */
	@GetMapping("/")
	public String index(Model model) {
		
		model.addAttribute("template", "forms/loginForm");
		return "index";
	}
	
	/**
	 * Loguear
	 * @param principal
	 * @param flash
	 * @param request
	 * @return
	 */
	@GetMapping("/login")
	public String loginPage(Principal principal, Model model, HttpServletRequest request) {
		
		if (principal != null) {
			model.addAttribute("info", "Ya tiene una sesión abierta.");
			return "redirect:" + request.getHeader("Referer"); // Back previous page
		}
		return "index";
	}
	
	/**
	 * Recoge un logueo erróneo
	 * @param model
	 * @return formulario de login
	 */
	@GetMapping("/login-error")
	public String loginError(Model model) {
		
		model.addAttribute("template", "forms/loginForm");
		return "index";
	}
	
	/**
	 * Método para generar una contraseña olvidada
	 * @param model
	 * @return
	 */
	@GetMapping("/forgetPassword")
	public String forgetPasswordPage (Model model) {
		
		model.addAttribute("template", "forms/forgetPasswordForm");
		return "index";
	}
	
	/**
	 * Genera una nueva contraseña y se manda un email al usuario
	 * @param model
	 * @param username
	 * @return
	 */
	@PostMapping("/forgetPassword")
	public String forgetPasswordPagePost (Model model, @RequestParam("username") String username) {
		
		//Se busca al usuario
		User user = userService.findUserByUsername(username);
		//si existe ese usuario, se genera contraseña y se manda email
		if(user != null) {
			
			String password = randomPassword ();
		    user.setPassword(passwordEncoder.encode(password));
			userService.saveUser2(user);
			//Obtenemos el email del usuario al que enviaremos la contraseña
			String toEmail = user.getEmail();
			String text = "Su nueva contraseña es: \n" + password;
			
			//Enviamos el email mediante un servicio
			
			mailService.sendMail("alula.proyecto@gmail.com", toEmail, "Nueva contraseña", text);
			model.addAttribute("info", "Hemos enviado su nueva contraseña a su cuenta de correo electrónico.");
			model.addAttribute("template", "forms/loginForm");
			
		}else {
			
			model.addAttribute("template", "forms/forgetPasswordForm");
			model.addAttribute("info", "El usuario no existe. Comprueba que has introducido el nombre de usuario correctamente.");
		}
		
		return "index";
	}
	
	/**
	 * Acceso a la página de bienvenida
	 * @param principal
	 * @param model
	 * @return
	 */
	@GetMapping("/welcome")
	public String welcomePage(Principal principal, Model model) {
		
		model.addAttribute("user", currentUser(principal));
		return "welcome";
	}
	
	/**
	 * Si el usuario tiene rol de admin enruta hacia el área de administrador, si no hacia la pizarra digital
	 * @param principal
	 * @param model
	 * @return
	 */
	@PostMapping("/welcome")
	public String welcomePagePost(Principal principal, Model model) {
		
		Role role = userService.findRoleByName("ADMIN");
		
		if(currentUser(principal).getRoles().contains(role)) {
			
			return "redirect:/admin/";
			
		}else {
			
			return "redirect:/dashboard";
		}
		
	}
	
	/**
	 * Recoge el acceso a la pizarra digital para redireccionar
	 * @param principal
	 * @param model
	 * @return redireccina según el rol
	 */
	@GetMapping("/dashboard")
	public String alulaPage(Principal principal) {
		
		Role role = userService.findRoleByName("STUDENT");
		
		if(currentUser(principal).getRoles().contains(role)) {
			return "redirect:/student/";
		}else {
			return "redirect:/teacher/";
		}
		
	}
	/**
	 * Genera un string aleatorio
	 * @return
	 */
	public String randomPassword () {
		
		int length = 5;
	    boolean letters = true;
	    boolean numbers = true;
	    String password = RandomStringUtils.random(length, letters, numbers);
	    
		return password;
	}
	
	/**
	 * Método para obtener el id del curso de un alumno
	 * @param principal
	 * @return id
	 */
	public Long courseId (Principal principal) {
		
		Long id;
		ArrayList<Course> courses = new ArrayList<Course>();
		for (Course course : currentUser(principal).getCourses()) {
			courses.add(course);
		}
		id = courses.get(0).getId();
		return id;
	}
	
	/**
	 * Se obtiene el nombre de la clase del alumno
	 * @param principal
	 * @return
	 */
	public String classroom (Principal principal) {
		
		String classroom;
		ArrayList<Classroom> classes = new ArrayList<Classroom>();
		for (Classroom classr: currentUser(principal).getClassrooms()) {
			classes.add(classr);
		}
		classroom = classes.get(0).getName();
		return classroom;
	}
	
}