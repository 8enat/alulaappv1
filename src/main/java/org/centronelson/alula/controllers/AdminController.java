package org.centronelson.alula.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.HashSet;
import java.util.UUID;

import javax.validation.Valid;

import org.centronelson.alula.models.entity.Classroom;
import org.centronelson.alula.models.entity.Course;
import org.centronelson.alula.models.entity.Role;
import org.centronelson.alula.models.entity.Subject;
import org.centronelson.alula.models.entity.User;
import org.centronelson.alula.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private UserService userService;
	
	/**
	 * Obtener el usuario actual
	 * @param principal
	 * @return
	 */
	private User currentUser(Principal principal) {
		return userService.findUserByUsername(principal.getName());
	}
	
	User admin = new User();
	Boolean isEdit;
	
	/**
	 * Acceso a la página del administrador
	 * @param principal
	 * @param model
	 * @return
	 */
	@GetMapping("/")
	public String adminPage(Principal principal, Model model) {
		
		admin = currentUser(principal);
		model.addAttribute("admin", admin);
		model.addAttribute("template", null);
	
		return "admin/home";
		
	}
	/**
	 * Obtener el listado de profesores
	 * @param model
	 * @return
	 */
	@GetMapping("/teachers")
	public String teachersList(Model model) {
		
		model.addAttribute("admin", admin);
		model.addAttribute("users", userService.findTeachers());
		model.addAttribute("title", "PROFESORES");
		model.addAttribute("template", "admin/usersAdmin");
		
		return "admin/home";
	}
	
	/**
	 * Obtener el listado de alumnos
	 * @param model
	 * @return
	 */
	@GetMapping("/students")
	public String studentsList(Model model) {
		
		model.addAttribute("admin", admin);
		model.addAttribute("users", userService.findStudents());
		model.addAttribute("title", "ALUMNOS");
		model.addAttribute("template", "admin/usersAdmin");
		return "admin/home";
	}
	
	/**
	 * Obtener el listado de administradores
	 * @param model
	 * @return
	 */
	@GetMapping("/admins")
	public String adminsList(Model model) {
		
		model.addAttribute("admin", admin);
		model.addAttribute("users", userService.findAdmins());
		model.addAttribute("title", "ADMINISTRADORES");
		model.addAttribute("template", "admin/usersAdmin");
		return "admin/home";
	}
	
	
	
	/**
	 * Obtener el listado de cursos
	 * @param model
	 * @return
	 */
	@GetMapping("/courses")
	public String coursesList(Model model) {
		
		model.addAttribute("admin", admin);
		model.addAttribute("courses", userService.findAllCourses());
		model.addAttribute("title", "CURSOS");
		model.addAttribute("template", "admin/coursesAdmin");
		return "admin/home";
	}
	
	/**
	 * Obtener el listado de clases
	 * @param model
	 * @return
	 */
	@GetMapping("/classrooms")
	public String classroomsList(Model model) {
		
		model.addAttribute("admin", admin);
		model.addAttribute("classrooms", userService.findAllClassroomByOrderByNameAsc());
		model.addAttribute("title", "CLASES");
		model.addAttribute("template", "admin/classroomsAdmin");
		return "admin/home";
	}
	
	/**
	 * Obtener el listado de asignaturas
	 * @param model
	 * @return
	 */
	@GetMapping("/subjects")
	public String subjectsList(Model model) {
		
		model.addAttribute("admin", admin);
		model.addAttribute("subjects", userService.findAllSubjectsByOrderByIdAsc());
		model.addAttribute("title", "ASIGNATURAS");
		model.addAttribute("template", "admin/subjectsAdmin");
		return "admin/home";
	}
	
	/**
	 * Método para acceder al formulario de añadir usuario
	 * @param model
	 * @return
	 */
	@GetMapping("/addUser")
	public String addUser(Model model) {
		
		isEdit = false;
		model.addAttribute("isEdit", isEdit);
		model.addAttribute("admin", admin);
		model.addAttribute("roles", userService.findTeacherStudentRoles());
		model.addAttribute("courses", userService.findAllCourses());
		model.addAttribute("classrooms", userService.findAllClassroomByOrderByNameAsc());
		model.addAttribute("subjects", userService.findAllSubjects());
		model.addAttribute("user", new User());
		model.addAttribute("template", "forms/addUserForm");
		
		return "admin/home";
	}
	
	/**
	 * Método para añadir un usuario con sus cursos y asignaturas correspondientes
	 * @param user
	 * @param binding
	 * @param model
	 * @param flash
	 * @param roles
	 * @param courses
	 * @param subjects
	 * @param picture
	 * @return
	 */
	@PostMapping("/addUser")
	public String addUser(@Valid User user, BindingResult binding, Model model, @RequestParam("role") HashSet<String> roles, @RequestParam("file") MultipartFile picture,
		@RequestParam("course") HashSet<String> courses, @RequestParam("classroom") HashSet<String> classrooms, @RequestParam("subject") HashSet<String> subjects) {
		
		if (binding.hasErrors()) {
			model.addAttribute("info", "Ha ocurrido un error");
			return "forms/addUserForm";
		}
		
		if(!picture.isEmpty()) {
			
			String uniqueFilename = UUID.randomUUID().toString() + "_" + picture.getOriginalFilename();
			Path rootPath = Paths.get("uploads/img").resolve(uniqueFilename);				
			try {					
				Files.copy(picture.getInputStream(), rootPath.toAbsolutePath());
				user.setPicture(uniqueFilename);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else {
			user.setPicture(user.getPicture());
		}
		
		HashSet <String> roleNames = roles;
		HashSet <Role> userRoles = new HashSet<Role>();
		Role rol = new Role();
		for(String role : roleNames) {
			rol = userService.findRoleByName(role);
			userRoles.add(rol);
		}
		user.setRoles(new HashSet<Role>(userRoles));
		
		//asignación de cursos
		
		HashSet <String> namesCourses = courses;
		HashSet <Course> userCourses = new HashSet<Course>();
		Course newCourse = new Course();
		for (String course : namesCourses) {
			newCourse =userService.findCourseByName(course);
			userCourses.add(newCourse);
		}
		user.setCourses(new HashSet<Course>(userCourses));
		
		//asignación de clases
		
		HashSet <String> namesClassrooms = classrooms;
		HashSet <Classroom> userClassrooms = new HashSet<Classroom>();
		Classroom newClassroom = new Classroom();
		for (String classr : namesClassrooms) {
			
			newClassroom = userService.findClassroomByName(classr);
			userClassrooms.add(newClassroom);
		}
		user.setClassrooms(new HashSet<Classroom>(userClassrooms));

		//asignación de asignaturas
		
		HashSet <String> namesSubjects = subjects;
		HashSet <Subject> userSubjects = new HashSet<Subject>();
		Subject newSubject = new Subject();
		for (String subject : namesSubjects) {
			newSubject =userService.findSubjectByName(subject);
			userSubjects.add(newSubject);
		}
		user.setSubjects(new HashSet<Subject>(userSubjects));
		
		userService.saveUser(user);
				
		model.addAttribute("admin", admin);
		model.addAttribute("template", null);
		model.addAttribute("info", "Acción realizada con éxito.");
		
		return "admin/home";
	}
	
	/**
	 * Recoge la ruta para modificar un usuario
	 * @param id
	 * @param model
	 * @return formulario de modificación
	 */
	@GetMapping("/updateUser/{id}")
	public String updateUser (@PathVariable("id") Long id, Model model) {
		
		User user = userService.findUserById(id).orElse(null);
		isEdit = true;
		Role role = userService.findRoleByName("ADMIN");
		if(user.getRoles().contains(role)) {
			
			model.addAttribute("template", "forms/addAdminForm");
			
		}else {
			model.addAttribute("roles", userService.findTeacherStudentRoles());
			model.addAttribute("courses", userService.findAllCourses());
			model.addAttribute("classrooms", userService.findAllClassroomByOrderByNameAsc());
			model.addAttribute("subjects", userService.findAllSubjects());
			model.addAttribute("template", "forms/addUserForm");
		}
		
		model.addAttribute("isEdit", isEdit);
		model.addAttribute("admin", admin);
		model.addAttribute("user", user);
	
		return "admin/home";
	}

	/**
	 * Método para eliminar un usuario
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/deleteUser/{id}")
	public String deleteUser (@PathVariable("id") Long id, Model model) {
		
		if(id == admin.getId() || id == null) {
			model.addAttribute("admin", admin);
			model.addAttribute("info", "Esta acción no puede realizarse.");
			model.addAttribute("template", "admin/usersAdmin");
			return "admin/home";
		}
		
		userService.deleteUser(id);
		model.addAttribute("admin", admin);
		//model.addAttribute("template", null);
		model.addAttribute("template", "admin/usersAdmin");
		model.addAttribute("info", "Usuario eliminado con éxito.");
		
		return "admin/home";
		
	}
	
	/**
	 * Obtener la ruta para añadir o editar un admin
	 * @param model
	 * @return
	 */
	@GetMapping("/addAdmin")
	public String addAdmin(Model model) {
		
		isEdit = false;
		model.addAttribute("isEdit", isEdit);
		model.addAttribute("admin", admin);
		model.addAttribute("user", new User());
		model.addAttribute("template", "forms/addAdminForm");
		
		return "admin/home";
	}
	
	/**
	 * Añade un administrador
	 * @param user
	 * @param binding
	 * @param model
	 * @param picture
	 * @return
	 */
	@PostMapping("/addAdmin")
	public String addAdmin (@Valid User user, BindingResult binding, Model model, @RequestParam("file") MultipartFile picture) {
		
		if (binding.hasErrors()) {
			model.addAttribute("info", "Ha ocurrido un error");
			return "forms/addAdminForm";
		}
		
		if(!picture.isEmpty()) {
			
			String uniqueFilename = UUID.randomUUID().toString() + "_" + picture.getOriginalFilename();
			Path rootPath = Paths.get("uploads/img").resolve(uniqueFilename);				
			try {					
				Files.copy(picture.getInputStream(), rootPath.toAbsolutePath());
				user.setPicture(uniqueFilename);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else {
			user.setPicture(user.getPicture());
		}
		
		HashSet <Role> userRoles = new HashSet<Role>();
		Role role = userService.findRoleByName("ADMIN");
		userRoles.add(role);
		user.setRoles(new HashSet<Role>(userRoles));
		
		userService.saveUser(user);
				
		model.addAttribute("admin", admin);
		model.addAttribute("template", null);
		model.addAttribute("info", "Acción realizada con éxito.");
		
		return "admin/home";
	}
	
	/**
	 * Edita un administrador recogiendo su id
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/updateAdmin/{id}")
	public String updateAdmin (@PathVariable("id") Long id, Model model) {
		
		User user = userService.findUserById(id).orElse(null);
		isEdit = true;
		model.addAttribute("isEdit", isEdit);
		model.addAttribute("admin", admin);
		model.addAttribute("user", user);
		model.addAttribute("template", "forms/addAdminForm");
		
		return "admin/home";
	}
	
	
	/**
	 * Recoge la ruta para ir al formulario de añadir un curso
	 * @param model
	 * @return
	 */
	@GetMapping("/addCourse")
	public String addCourse (Model model) {
		
		isEdit = false;
		model.addAttribute("isEdit", isEdit);
		model.addAttribute("admin", admin);
		model.addAttribute("classrooms", userService.findAllClassroomByOrderByNameAsc());
		model.addAttribute("subjects", userService.findAllSubjects());
		model.addAttribute("course", new Course());
		model.addAttribute("template", "forms/addCourse");
		
		return "admin/home";
	}
	
	/**
	 * Recoge la ruta para modificar un curso
	 * @param id
	 * @param model
	 * @return formulario
	 */
	@GetMapping("/updateCourse/{id}")
	public String updateCourse (@PathVariable("id") Long id, Model model) {
		
		Course course = userService.findCourseById(id);
		
		isEdit = true;
		model.addAttribute("isEdit", isEdit);
		model.addAttribute("admin", admin);
		model.addAttribute("classrooms", userService.findAllClassroomByOrderByNameAsc());
		model.addAttribute("subjects", userService.findAllSubjects());
		model.addAttribute("course", course);
		model.addAttribute("template", "forms/addCourse");
		
		return "admin/home";
	}
	
	/**
	 * Añade un curso
	 * @param course
	 * @param binding
	 * @param model
	 * @param classrooms
	 * @param subjects
	 * @return
	 */
	@PostMapping("/addCourse")
	public String addUserCourses(@Valid Course course, BindingResult binding, Model model, @RequestParam("classrooms") HashSet<String> classrooms, @RequestParam("subject") HashSet<String> subjects) {
		
		HashSet <String> namesClassrooms = classrooms;
		HashSet <Classroom> courseClassrooms = new HashSet<Classroom>();
		Classroom newClassroom = new Classroom();
		for (String classr : namesClassrooms) {
			
			newClassroom = userService.findClassroomByName(classr);
			courseClassrooms.add(newClassroom);
		}
		course.setClassrooms(new HashSet<Classroom>(courseClassrooms));

		//asignación de asignaturas
		
		HashSet <String> namesSubjects = subjects;
		HashSet <Subject> courseSubjects = new HashSet<Subject>();
		Subject newSubject = new Subject();
		for (String subject : namesSubjects) {
			newSubject = userService.findSubjectByName(subject);
			courseSubjects.add(newSubject);
		}
		course.setSubjects(new HashSet<Subject>(courseSubjects));
		
		userService.saveCourse(course);
		
		model.addAttribute("admin", admin);
		model.addAttribute("template", null);
		model.addAttribute("info", "Acción realizada con éxito.");
		
		return "admin/home";
		
	}
	
	/**
	 * Método para eliminar un curso
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/deleteCourse/{id}")
	public String deleteCourse (@PathVariable("id") Long id, Model model) {
		
		userService.deleteCourse(id);
		model.addAttribute("admin", admin);
		model.addAttribute("template", "admin/coursesAdmin");
		model.addAttribute("info", "Curso eliminado con éxito.");
		
		return "admin/home";
		
	}
	
	
	/**
	 * Recoge la ruta para ir al formulario de añadir una clase
	 * @param model
	 * @return
	 */
	@GetMapping("/addClassroom")
	public String addClassroom (Model model) {
		
		isEdit = false;
		model.addAttribute("isEdit", isEdit);
		model.addAttribute("admin", admin);
		model.addAttribute("classroom", new Classroom());
		model.addAttribute("template", "forms/addClassroom");
		
		return "admin/home";
	}
	
	/**
	 * Recoge la ruta para modificar una clase
	 * @param id
	 * @param model
	 * @return formulario de modificación
	 */
	@GetMapping("/updateClassroom/{id}")
	public String updateClassroom (@PathVariable("id") Long id, Model model) {
		
		Classroom classroom = userService.findClassroomById(id);
		
		isEdit = true;
		model.addAttribute("isEdit", isEdit);
		model.addAttribute("admin", admin);
		model.addAttribute("classroom", classroom);
		model.addAttribute("template", "forms/addClassroom");
		
		return "admin/home";
	}
	
	
	/**
	 * Añade una nueva clase.
	 * @param classroom
	 * @param binding
	 * @param model
	 * @param status
	 * @return
	 */
	@PostMapping("/addClassroom")
	public String addClassroom (@Valid Classroom classroom, BindingResult binding, Model model, SessionStatus status) {
		
		if (binding.hasErrors()) {
			model.addAttribute("info", "Ha ocurrido un error");
			model.addAttribute("admin", admin);
			model.addAttribute("template", "admin/usersAdmin");
			return "admin/home";
		}
		
		
		userService.saveClassroom(classroom);
		
		status.setComplete();
		
		model.addAttribute("admin", admin);
		model.addAttribute("template", null);
		model.addAttribute("info", "Acción realizada con éxito.");
		
		return "admin/home";
	}
	
	/**
	 * Método para eliminar una clase
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/deleteClassroom/{id}")
	public String deleteClassroom (@PathVariable("id") Long id, Model model) {
		
		userService.deleteClassroom(id);
		model.addAttribute("admin", admin);
		model.addAttribute("template", "admin/classroomsAdmin");
		model.addAttribute("info", "Clase eliminada con éxito.");
		
		return "admin/home";
		
	}
	
	
	/**
	 * Recoge la ruta para ir al formulario de añadir una asignatura
	 * @param model
	 * @return
	 */
	@GetMapping("/addSubject")
	public String addSubject (Model model) {
		
		isEdit = false;
		model.addAttribute("isEdit", isEdit);
		model.addAttribute("admin", admin);
		model.addAttribute("subject", new Subject());
		model.addAttribute("template", "forms/addSubject");
		
		return "admin/home";
	}
	
	/**
	 * Recoge la ruta para modificar una asignatura
	 * @param id
	 * @param model
	 * @return formulario de modificación
	 */
	@GetMapping("/updateSubject/{id}")
	public String updateSubject (@PathVariable("id") Long id, Model model) {
		
		Subject subject = userService.findSubjectById(id);
		
		isEdit = true;
		model.addAttribute("isEdit", isEdit);
		model.addAttribute("admin", admin);
		model.addAttribute("isEdit", isEdit);
		model.addAttribute("subject", subject);
		model.addAttribute("template", "forms/addSubject");
		
		return "admin/home";
	}
	
	/**
	 * Añade una asignatura
	 * @param subject
	 * @param binding
	 * @param model
	 * @param picture
	 * @return
	 */
	@PostMapping("/addSubject")
	public String addSubject (@Valid Subject subject, BindingResult binding, Model model, @RequestParam("file") MultipartFile picture) {
		
		if (binding.hasErrors()) {
			model.addAttribute("info", "Ha ocurrido un error");
			return "forms/addSubject";
		}
		
		if(!picture.isEmpty()) {
			
			String uniqueFilename = UUID.randomUUID().toString() + "_" + picture.getOriginalFilename();
			Path rootPath = Paths.get("uploads/img").resolve(uniqueFilename);				
			try {					
				Files.copy(picture.getInputStream(), rootPath.toAbsolutePath());
				subject.setPicture(uniqueFilename);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		userService.saveSubject(subject);
		
		model.addAttribute("admin", admin);
		model.addAttribute("template", null);
		model.addAttribute("info", "Acción realizada con éxito.");
		
		return "admin/home";
	}
	
	/**
	 * Método para eliminar una asignatura
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/deleteSubject/{id}")
	public String deleteSubject (@PathVariable("id") Long id, Model model) {
		
		userService.deleteSubject(id);
		model.addAttribute("admin", admin);
		model.addAttribute("template", "admin/subjectsAdmin");
		model.addAttribute("info", "Asignatura eliminada con éxito.");
		
		return "admin/home";
		
	}
}
