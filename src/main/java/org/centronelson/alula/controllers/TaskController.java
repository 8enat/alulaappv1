package org.centronelson.alula.controllers;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import org.centronelson.alula.models.dao.TaskRepository;
import org.centronelson.alula.models.dao.UserRepository;
import org.centronelson.alula.models.entity.Subject;
import org.centronelson.alula.models.entity.Task;
import org.centronelson.alula.models.entity.User;
import org.centronelson.alula.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class TaskController {
	@Autowired
	private TaskRepository taskRepository;
	@Autowired
	private UserRepository userRepository;

	
	@Autowired
	private UserService userService;
	
	
	/**
	 * Devolverá a los alumnos que tengan dicha tarea asignada
	 */
	@GetMapping("/summaryTasks/{id}")
	public String summaryTasks(@PathVariable("id") Long id, Model model, Principal principal) {
		model.addAttribute("idTask",id);
		model.addAttribute("users",userRepository.findByTasksId(id));
		model.addAttribute("task", taskRepository.findById(id).orElse(null));
		model.addAttribute("user", currentUser(principal));
		model.addAttribute("tasks", taskRepository.findAll());
		model.addAttribute("template","summaryTasks");
		return "AlulaIndex";
		
	}
	
	
	/**
	 * Obtener el usuario actual
	 * @param principal
	 * @return
	 */
	private User currentUser(Principal principal) {
		return userService.findUserByUsername(principal.getName());
	}
}
