package org.centronelson.alula.controllers;


import java.security.Principal;
import java.util.ArrayList;

import org.centronelson.alula.models.entity.Classroom;
import org.centronelson.alula.models.entity.Learning;
import org.centronelson.alula.models.entity.Role;
import org.centronelson.alula.models.entity.User;
import org.centronelson.alula.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;


@Controller
@SessionAttributes("user")
@RequestMapping("/user")
public class UserController implements Learning{

	@Autowired
	private UserService userService;
	
	@Autowired
    private BCryptPasswordEncoder passwordEncoder;
	
	private User currentUser;
	private String studentClassroom;
	
	/**
	 * Obtener el usuario actual
	 * @param principal
	 * @return
	 */
	private User currentUser(Principal principal) {
		return userService.findUserByUsername(principal.getName());
	}
	
	/**
	 * Acceso al perfil de cada usuario
	 * @param principal
	 * @param model
	 * @return perfil
	 */
	@GetMapping("/profile")
	public String profilePage(Principal principal, Model model) {
		
		currentUser = currentUser(principal);
		Role role = userService.findRoleByName("STUDENT");
		
		if(currentUser.getRoles().contains(role)) {
			studentClassroom = classroom(principal);
			model.addAttribute("classroom", studentClassroom);
		}
		
		model.addAttribute("user", currentUser);
		model.addAttribute("info", "Mi perfil");
		model.addAttribute("template", "profile");
		
		return "AlulaIndex";
	}
	
	/**
	 * Acceso al formulario para cambiar la contraseña del usuario
	 * @param model
	 * @return
	 */
	@GetMapping("/newPassword")
	public String newPassword(Model model) {
		
		model.addAttribute("user", currentUser);
		model.addAttribute("template", "forms/newPasswordForm");
		
		return "AlulaIndex";
	}
	
	/**
	 * Formulario para cambiar la contraseña
	 * @param principal
	 * @param model
	 * @param currentPassword
	 * @param newPassword
	 * @return
	 */
	@PostMapping("/newPassword")
	public String newPassword(Principal principal, Model model, @RequestParam("currentPassword") String currentPassword,
			@RequestParam("newPassword") String newPassword) {
		
		if(passwordEncoder.matches(currentPassword, currentUser.getPassword())) {
			
			currentUser.setPassword(passwordEncoder.encode(newPassword));
			userService.saveUser2(currentUser);
			model.addAttribute("user", currentUser);
			model.addAttribute("info", "Contraseña cambiada correctamente.");
			model.addAttribute("template", "profile");
			
		}else {
			
			model.addAttribute("user", currentUser);
			model.addAttribute("info", "Contraseña actual incorrecta.");
			model.addAttribute("template", "forms/newPasswordForm");
			
		}
		
		return "AlulaIndex";
	}
	
	/**
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/detail/{id}")
	public String showDetails(@PathVariable("id") Long id, Model model) {
		model.addAttribute("user", userService.findById(id).orElse(null));
		return "details";
	}

	@Override
	public String selectSubject(String name, Principal principal, Model model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long courseId(Principal principal) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String classroom(Principal principal) {
		// TODO Auto-generated method stub
		String classroom;
		ArrayList<Classroom> classes = new ArrayList<Classroom>();
		for (Classroom classr: currentUser(principal).getClassrooms()) {
			classes.add(classr);
		}
		classroom = classes.get(0).getName();
		return classroom;
	}
	
	
	
}
