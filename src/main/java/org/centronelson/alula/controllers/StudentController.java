package org.centronelson.alula.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.centronelson.alula.models.entity.Classroom;
import org.centronelson.alula.models.entity.Course;
import org.centronelson.alula.models.entity.Learning;
import org.centronelson.alula.models.entity.Subject;
import org.centronelson.alula.models.entity.User;
import org.centronelson.alula.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/student")
public class StudentController implements Learning{
	
	@Autowired
	private UserService userService;
	
	private User currentUser;
	private String studentClassroom;
	private Long studentSubjectId;
	
	/**
	 * Obtener el usuario actual
	 * @param principal
	 * @return
	 */
	private User currentUser(Principal principal) {
		return userService.findUserByUsername(principal.getName());
	}
	
	/**
	 * Recoge el acceso a student y permite seleccionar asignatura
	 * @param principal
	 * @param model
	 * @return AlulaIndex
	 */
	@GetMapping("/")
	public String index(Principal principal, Model model) {
		
		currentUser = currentUser(principal);
		Long courseId = courseId(principal);
		Course course = userService.findCourseById(courseId);
		studentClassroom = classroom(principal);
		
		model.addAttribute("user", currentUser);
		model.addAttribute("course", course);
		model.addAttribute("classroom", studentClassroom);
		model.addAttribute("subjects", userService.findSubjectsByUserAndCourse(currentUser(principal).getUsername(), course.getName()));
		model.addAttribute("info", "Selecciona asignatura.");
		model.addAttribute("template", "selectSubject");
		
		return "AlulaIndex";
	}
	
	@Override
	@GetMapping("/subject/{name}")
	public String selectSubject(@PathVariable("name") String name, Principal principal, Model model) {
		
		Subject subject = userService.findSubjectByName(name);
		studentSubjectId = subject.getId();
		
		model.addAttribute("user", currentUser(principal));
		model.addAttribute("classroom", studentClassroom);
		model.addAttribute("subject", subject);
		model.addAttribute("info", "Has seleccionado " + subject.getName() + ".");
		model.addAttribute("template", "wip");//tendrá que ir dónde corresponda en vez de a "pruebas"
		
		return "AlulaIndex";
	}
	

	@Override
	public Long courseId(Principal principal) {
		// TODO Auto-generated method stub
		Long id;
		ArrayList<Course> courses = new ArrayList<Course>();
		for (Course course : currentUser(principal).getCourses()) {
			courses.add(course);
		}
		id = courses.get(0).getId();
		return id;
	}

	@Override
	public String classroom(Principal principal) {
		// TODO Auto-generated method stub
		String classroom;
		ArrayList<Classroom> classes = new ArrayList<Classroom>();
		for (Classroom classr: currentUser(principal).getClassrooms()) {
			classes.add(classr);
		}
		classroom = classes.get(0).getName();
		return classroom;
	}


	
	
}
