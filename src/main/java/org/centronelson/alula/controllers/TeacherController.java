package org.centronelson.alula.controllers;

import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.centronelson.alula.models.dao.MessaggeRepository;
import org.centronelson.alula.models.dao.TaskRepository;
import org.centronelson.alula.models.dao.UserRepository;
import org.centronelson.alula.models.entity.Attendance;
import org.centronelson.alula.models.entity.Classroom;
import org.centronelson.alula.models.entity.Course;
import org.centronelson.alula.models.entity.Learning;
import org.centronelson.alula.models.entity.Messagge;
import org.centronelson.alula.models.entity.Subject;
import org.centronelson.alula.models.entity.Task;
import org.centronelson.alula.models.entity.User;
import org.centronelson.alula.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/teacher")
public class TeacherController implements Learning{
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private MessaggeRepository messaggeRepository;
	
	@Autowired
	private TaskRepository taskRepository;
	
	@Autowired
	private UserService userService;
	
	private User currentUser;
	private Long teacherSubjectId;
	private String teacherCourseClass;
	

	/**
	 * Obtener el usuario actual
	 * @param principal
	 * @return
	 */
	private User currentUser(Principal principal) {
		return userService.findUserByUsername(principal.getName());
	}
	
	@GetMapping("/")
	public String index(Principal principal, Model model) {
		
		currentUser = currentUser(principal);
		
		List<Classroom> allClasses = userService.findAllClassroomByOrderByNameAsc();
		List<Classroom> classrooms = new ArrayList<Classroom>();
		for (Classroom classroom : allClasses) {
			if(currentUser(principal).getClassrooms().contains(classroom)) {
				classrooms.add(classroom);
			}
		}
		
		model.addAttribute("user", currentUser);
		model.addAttribute("classrooms", classrooms);
		model.addAttribute("info", "Selecciona curso y clase.");
		model.addAttribute("template", "forms/courseClassForm");
		
		return "AlulaIndex";
		
	}
	
	
	/**
	 * Método para seleccionar la clase en la pizarra y cargar las asignaturas correspondientes
	 * @param principal
	 * @param model
	 * @param className
	 * @return página principal
	 */
	@PostMapping("/courseClass")
	public String SelectSubjectFromForm(Principal principal, Model model, @RequestParam("classroom") String className) {
		
		teacherCourseClass = className;
		selectSubject(principal, model);
		
		return "AlulaIndex";
		
	}
	
	/**
	 * Método para seleccionar la clase en la pizarra y cargar las asignaturas correspondientes
	 * @param principal
	 * @param model
	 * @return
	 */
	@GetMapping("/subject")
	public String selectSubjectFromButton(Principal principal, Model model) {
		
		selectSubject(principal, model);
		
		return "AlulaIndex";
		
	}
	
	/**
	 * Devuelve la lista de las tareas
	 * @param principal
	 * @param model
	 * @return
	 */
	@GetMapping("/listTask")
	public String listTask(Principal principal, Model model) {
		Subject subject = userService.findSubjectByName(userService.findSubjectById(teacherSubjectId).getName());
		teacherSubjectId = subject.getId();
		
		model.addAttribute("user", currentUser(principal));
		model.addAttribute("subjectId", teacherSubjectId);
		model.addAttribute("tasks", taskRepository.findBySubjectId(teacherSubjectId));
		ArrayList<Task> numTask= taskRepository.findBySubjectId(teacherSubjectId);
		model.addAttribute("numTask",numTask.size());
		model.addAttribute("template","listTask");
		return "AlulaIndex";
	}
	
	/**
	 * Método que añade los atributos al modelo necesarios para la vista
	 * @param principal
	 * @param model
	 */
	public void selectSubject(Principal principal, Model model) {
		
		Classroom classroom = userService.findClassroomByName(teacherCourseClass);
		
		Course course = null;
		for (Classroom classr : currentUser(principal).getClassrooms()) {
			if (classr.getId() == classroom.getId()) {
				course = userService.findByClassroomId(classroom.getId());
			}
		}
		
		model.addAttribute("user", currentUser(principal));
		model.addAttribute("course", course);
		model.addAttribute("classroom", classroom);
		model.addAttribute("subjects", userService.findSubjectsByUserAndCourse(currentUser(principal).getUsername(), course.getName()));
		model.addAttribute("info", "Has seleccionado " + classroom.getName() + ".");
		model.addAttribute("template", "selectSubject");
	}
	
	/**
	 * Selecciona la asignatura por el nombre pasado por parámetro en la URL
	 * @param name
	 * @param principal
	 * @param model
	 * @return página principal
	 */
	@GetMapping("/subject/{name}")
	public String selectSubject(@PathVariable("name") String name, Principal principal, Model model) {
		
		Subject subject = userService.findSubjectByName(name);
		teacherSubjectId = subject.getId();
		List<Long> ids=userService.findStudentsIdsBySubjectId(teacherSubjectId);
		List<User> students=new ArrayList<User>();
		Optional<User> us=null;
		User usAux=null;
		for(Long a: ids) {
			us=userService.findUserById(a);
			usAux=us.get();
			students.add(usAux);
		}
		
		model.addAttribute("user", currentUser(principal));
		model.addAttribute("subject", subject);
		model.addAttribute("classroom", teacherCourseClass);
		model.addAttribute("info", "Has seleccionado " + subject.getName() + ".");
		model.addAttribute("users",students);
		model.addAttribute("template", "userList");
		
		return "redirect:/teacher/studentsList";
	}
	
	
	/**
	 * Este metodo sirve para devolver la lista de alumnos por curso y clase
	 * @param principal
	 * @param model
	 * @return
	 */
	@GetMapping("/studentsList")
	public String studentList( Principal principal, Model model) {
		
		Subject subject = userService.findSubjectById(teacherSubjectId);
		
		teacherSubjectId = subject.getId();
		List<Long> ids=userService.findStudentsIdsBySubjectId(teacherSubjectId);
		List<User> students=new ArrayList<User>();
		User usAux=null;
		Classroom classroom = userService.findClassroomByName(teacherCourseClass);
		for(Long a: ids) {
			usAux=userService.findUserById(a).get();
			if(usAux.getClassrooms().contains(classroom)) {
				students.add(usAux);
			}
		}
		
		model.addAttribute("user", currentUser(principal));
		model.addAttribute("subjectId", teacherSubjectId);
		model.addAttribute("subject", subject);
		model.addAttribute("classroom", teacherCourseClass);
		model.addAttribute("template","/listTask/"+teacherSubjectId);
//		model.addAttribute("taskId", "/listTask/"+teacherSubjectId);
		model.addAttribute("info", "Has seleccionado " + subject.getName() + ".");
		model.addAttribute("users",students);
		model.addAttribute("template", "userList");
		
		return "AlulaIndex";
	}
	/**
	 * Este metodo sirve para almacenar la falta de un alumno
	 * @param principal
	 * @param model
	 * @param id
	 * @return
	 */
	@GetMapping("/list/{id}")
	public String studentsAttendance(Principal principal, Model model,@PathVariable(value = "id") Long id) {
		Optional<User> us=userService.findUserById(id);
		LocalDateTime now=LocalDateTime.now();		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		String formatDateTime = now.format(formatter);
		User usAux=us.get();
		
		List<Long> ids=userService.findStudentsIdsBySubjectId(teacherSubjectId);
		model.addAttribute("template","/listTask/"+teacherSubjectId);
//		model.addAttribute("taskId", "/listTask/" + teacherSubjectId);
		List<User> students=new ArrayList<User>();
		Optional<User> userAuxOpt=null;
		User user=null;
		for(Long a: ids) {
			
			userAuxOpt=userService.findUserById(a);
			user=userAuxOpt.get();
			
			students.add(user);
		}
		Attendance att=new Attendance();
		long a= 1;
		att.setUserId(id);
		att.setDateAtt(formatDateTime);
		model.addAttribute("user", currentUser(principal));
		model.addAttribute("info","Falta añadida con exito a "+userService.findUserById(id).get().getName());
		userService.addAttendance(att);
		model.addAttribute("subjectId", teacherSubjectId);
		model.addAttribute("users",students);
		model.addAttribute("template", "userList");

		return "AlulaIndex";
	}
	
	/**
	 * Este método devuelve El formulario para enviar un mesaje directo a un alumno
	 * @param message
	 * @param principal
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/studentMessage/{id}")
	public String getMessageForm(@Valid Messagge message, Principal principal,@PathVariable(value = "id") Long id,Model model) {
		model.addAttribute("user", currentUser(principal));
		model.addAttribute("taskId", "/summaryTask/"+id);
		model.addAttribute("userOrig", currentUser(principal).getId());
		model.addAttribute("userDest", userService.findById(id).get().getId());
		model.addAttribute("template","sendMessage");
		
		return "AlulaIndex";
	}
	
	/**
	 * Este metodo Almacena un mensaje enviado a un usuario
	 * @param message
	 * @param principal
	 * @param id
	 * @param model
	 * @return
	 */
	@PostMapping("/studentMessage/{id}")
	public String postMessageForm(Principal principal,@PathVariable(value = "id") Long id,Model model,
			@RequestParam("userOrig") Long userOrig,@RequestParam("userDest") Long userDest,@RequestParam("messagge") String messagge,@RequestParam("subject") String subject) {
		List<Long> ids=userService.findStudentsIdsBySubjectId(teacherSubjectId);
		model.addAttribute("template","/listTask/"+teacherSubjectId);

		List<User> students=new ArrayList<User>();
		Optional<User> userAuxOpt=null;
		User user=null;
		for(Long a: ids) {
			
			userAuxOpt=userService.findUserById(a);
			user=userAuxOpt.get();
			
			students.add(user);
		}
		User us=new User();
		Messagge mess=new Messagge();
		mess.setMessagge(messagge);
		mess.setSubject(subject);
		mess.setId((long) userService.findMessagesCountByUserOrigAndUserDest(userService.findById(userOrig).get(), userService.findById(userDest).get()));
		us=userService.findById(userOrig).get();
		mess.setUserDest(us);
		us=userService.findById(userDest).get();
		mess.setUserOrig(us);
		userService.saveMessage(mess);
		model.addAttribute("info","Mensaje enviado a: "+currentUser(principal).getName());
		model.addAttribute("users",students);
		model.addAttribute("template", "userList");
		model.addAttribute("subjectId", teacherSubjectId);
		model.addAttribute("user", currentUser(principal));
		return "AlulaIndex";
	}


	/**
	 * Este metodo nos muestra los detalles de un alumno seleccionado
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/studentDetail/{id}")
	public String getStudentsDetails(Principal principal,@PathVariable(value = "id") Long id,Model model) {
		Optional<User> us=userService.findUserById(id);
		User usAux=new User();
		usAux=us.get();
		List<Attendance> attendancesByUserId=userService.findAllAttendanceByUserId(id);
		for(Attendance att:attendancesByUserId) {
			att.getDateAtt();
		}
		model.addAttribute("user",usAux);
		User user = userRepository.findById(id).orElse(null);
		User teacher = currentUser(principal);	
		model.addAttribute("teacherName",teacher.getName());
		model.addAttribute("teacherPicture",teacher.getPicture());
		model.addAttribute("numMessage",messaggeRepository.findByUserOrigAndUserDest(user, teacher).size());
		model.addAttribute("numAttendances",attendancesByUserId.size());
		model.addAttribute("taskId",  "/summaryTask/"+
		id);
		System.out.println("Numero de mensajes: "+messaggeRepository.findByUserOrigAndUserDest(user, teacher).size());
		model.addAttribute("messagges", messaggeRepository.findByUserOrigAndUserDest(user, teacher));
		model.addAttribute("user", teacher);
		model.addAttribute("student", user);
		model.addAttribute("template","studentDetail");
		model.addAttribute("attendances",userService.findAllAttendanceByUserId(id));
		return "AlulaIndex";
	}
	
	/**
	 * Devolverá la vista del alumno seleccionado para ver su perfil
	 * 
	 * @param id
	 * @param model
	 * @return studentData
	 */
	@GetMapping("/studentData/{id}")
	public String studentData(@PathVariable("id") Long id, Model model, Principal principal) {
		User user = userRepository.findById(id).orElse(null);
		User teacher = currentUser(principal);
		model.addAttribute("user", user);
		model.addAttribute("messagges", messaggeRepository.findByUserOrigAndUserDest(user, teacher));
		ArrayList<Messagge> messaggesUser=messaggeRepository.findByUserOrigAndUserDest(user, teacher);
		model.addAttribute("template","studentData");
		model.addAttribute("numMessage",messaggesUser.size());
		System.out.println("Numero de mensajes: "+ messaggesUser.size());
		return "AlulaIndex";
	}

	
	
	
	@Override
	public Long courseId(Principal principal) {
		// TODO Auto-generated method stub
		Long id;
		ArrayList<Course> courses = new ArrayList<Course>();
		for (Course course : currentUser(principal).getCourses()) {
			courses.add(course);
		}
		id = courses.get(0).getId();
		return id;
	}

	@Override
	public String classroom(Principal principal) {
		// TODO Auto-generated method stub
		String classroom;
		ArrayList<Classroom> classes = new ArrayList<Classroom>();
		for (Classroom classr: currentUser(principal).getClassrooms()) {
			classes.add(classr);
		}
		classroom = classes.get(0).getName();
		return classroom;
	}
	
	/**
	 * Metodo para devolver formulario de añadir tarea con la asignatura  y en curso en el que esta y los alumnos que hay  
	 * @param id
	 * @param model
	 * @param principal
	 * @return
	 */
	@GetMapping("/addTask/{id}")
	public String getaddTask(@PathVariable("id") Long id,Model model,Principal principal) {
		List<Long> ids=userService.findStudentsIdsBySubjectId(teacherSubjectId);
		List<User> students=new ArrayList<User>();
		for (User user:getStudentsByteacherSubjectId(ids)) {
			
		}
		User usAux=null;
		Classroom classroom = userService.findClassroomByName(teacherCourseClass);
		for(Long a: ids) {
			usAux=userService.findUserById(a).get();
			if(usAux.getClassrooms().contains(classroom)) {
				students.add(usAux);
			}
		}
		model.addAttribute("subject",userService.findSubjectById(teacherSubjectId));
		model.addAttribute("students", students);
		model.addAttribute("template","forms/addTask");
		model.addAttribute("user", currentUser(principal));
		return "AlulaIndex";
	}
	/**
	 * Este método almacena la tarea que ha guardado un profesor y se la asigna a un alumno
	 * @param model
	 * @param principal
	 * @param completed
	 * @param name
	 * @param description
	 * @param score
	 * @param type
	 * @param creationDate
	 * @param deadline
	 * @param subject
	 * @param studentId
	 * @return
	 * @throws ParseException
	 */
	@PostMapping("/addTasks")
	public String postaddTask(Model model,Principal principal,
			@RequestParam("completed")String completed,@RequestParam("name")String name,@RequestParam("description")String description,
			@RequestParam("score")String score,@RequestParam("type")String type,@RequestParam("creationDate")String creationDate,
			@RequestParam("deadline")String deadline,@RequestParam("subject") String subject,@RequestParam("student") String studentId
			) throws ParseException {
		
			//Método para recuperar la tarea del formulario,
			//Añadirle la asignatura y guardarla en la base de datos
			Task taskAux=new Task();		
			taskAux.setSubject(userService.findSubjectByName(subject));
			taskAux.setName(name);
			taskAux.setDescription(description);
			taskAux.setScore(score);
			taskAux.setType(type);
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-DD"); 
			Date date = (Date)formatter.parse(creationDate);
			taskAux.setCreationDate(date);
			formatter = new SimpleDateFormat("yyyy-MM-DD"); 
			date = (Date)formatter.parse(deadline);
			taskAux.setDeadline(date);
			taskAux.setCompleted(Boolean.parseBoolean(completed));
			userService.saveTask(taskAux);
			//En esta parte le asignamos la tarea al usuario 
			String userId=studentId.split(":")[0];
			
			User user = userService.findUserById(Long.parseLong(userId)).get();
			Set<Task>tasks=user.getTasks();
			tasks.add(taskAux);
			user.setTasks(tasks);
			userService.saveUser(user);
	
			return "redirect:/teacher/listTask";
		
		
		
	}
	
	/**
	 * Este metodo sirve para devolver los Estudiantes por el Id de la asignatura
	 * @param ids
	 * @return
	 */
	public List<User> getStudentsByteacherSubjectId(List<Long> ids){
		List<User> students=new ArrayList<User>();
		Optional<User> userAuxOpt=null;
		User user=null;
		for(Long a: ids) {			
			userAuxOpt=userService.findUserById(a);
			user=userAuxOpt.get();			
			students.add(user);
		}
		
		return students;
		
	}
	
	
}

