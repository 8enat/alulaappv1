package org.centronelson.alula.configurations;

import javax.sql.DataSource;

import org.centronelson.alula.models.dao.UserRepository;
import org.centronelson.alula.models.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private DataSource dataSource;

	@Value("${spring.queries.users-query}")
	private String usersQuery;

	@Value("${spring.queries.roles-query}")
	private String rolesQuery;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
		.jdbcAuthentication()
			.usersByUsernameQuery(usersQuery)
			.authoritiesByUsernameQuery(rolesQuery)
			.dataSource(dataSource)
			.passwordEncoder(passwordEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
	    
	    http
		    .sessionManagement()
		 	.sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
		 	.and()
	     .authorizeRequests()
	     	.antMatchers("/").permitAll()
	     	.antMatchers("/login").permitAll()
	     	.antMatchers("/forgetPassword").permitAll()
	     	.antMatchers("/pruebas").permitAll()
	     	.antMatchers("/results").permitAll()
	     	.antMatchers("/console/**").permitAll()
	     	.antMatchers("/register").permitAll()
	     	.antMatchers("/admin/**").hasAuthority("ADMIN")
	     	.antMatchers("/user/**").hasAnyAuthority("ADMIN", "TEACHER", "STUDENT", "PARENT")
	     	.antMatchers("/teacher/**").hasAuthority("TEACHER")
	     	.antMatchers("/student/**").hasAuthority("STUDENT")
	     	.antMatchers("/dashboard/**").hasAnyAuthority("STUDENT", "TEACHER")
	     	.anyRequest()
	     	.authenticated()
	     	.and()
	     .csrf().disable()
	     .formLogin()
	     	.loginPage("/login")
	     	.failureUrl("/login-error")
	     	.permitAll()
	     	.defaultSuccessUrl("/welcome")
	     	.and()
	     .csrf().disable()
	     	.headers().frameOptions().disable()
	     	.and()
	     .logout()
	     	.permitAll()
	     	.logoutSuccessUrl("/")
	     	.and()
	     .exceptionHandling().accessDeniedPage("/error403");
	  }
	
		@Override
		public void configure(WebSecurity web) throws Exception {
			web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/src/**", "/img/**", "/uploads/**");
		}

}