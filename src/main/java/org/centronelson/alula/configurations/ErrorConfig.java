package org.centronelson.alula.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Clase para mostrar error por acceso denegado
 * 
 * @author 404 not found
 *
 */
@Configuration
public class ErrorConfig implements WebMvcConfigurer {
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/error403").setViewName("errors/error403");
	}
}
