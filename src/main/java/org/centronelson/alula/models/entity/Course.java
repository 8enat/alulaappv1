package org.centronelson.alula.models.entity;


import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
/**
 * Tabla de Cursos 
 * @author 404 not found
 *
 */
@Entity(name = "Course")
@Table(name="course")
public class Course{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	
	@ManyToMany(cascade = {
		    CascadeType.PERSIST,
		    CascadeType.MERGE
		})
	@JoinTable(name = "course_classroom", joinColumns = @JoinColumn(name = "course_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "classroom_id", referencedColumnName = "id"))
	private Set<Classroom> classrooms;
	
	@ManyToMany(cascade = {
		    CascadeType.PERSIST,
		    CascadeType.MERGE
		})
	@JoinTable(name = "course_subject", joinColumns = @JoinColumn(name = "course_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "subject_id", referencedColumnName = "id"))
	private Set <Subject>subjects;
	
	/* Getters y Setters */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}
	public Set<Subject> getSubjects() {
		return subjects;
	}
	
	public Set<Classroom> getClassrooms() {
		return classrooms;
	}
	public void setClassrooms(Set<Classroom> classrooms) {
		this.classrooms = classrooms;
	}
	@Override
	public String toString() {
		return "Course [id=" + id + ", name=" + name + ", classes=" + classrooms + ", subjects=" + subjects + "]";
	}
	
	
	
	
	
	
	
}
