package org.centronelson.alula.models.entity;

import java.security.Principal;
import java.util.List;

import org.springframework.ui.Model;

public interface Learning {
	
	String selectSubject(String name, Principal principal, Model model);
	Long courseId (Principal principal);
	String classroom (Principal principal);
	

}
