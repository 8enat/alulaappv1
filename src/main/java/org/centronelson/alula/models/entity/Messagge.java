package org.centronelson.alula.models.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Tabla de mensajes
 * 
 * @author 404 not found
 *
 */
@Entity(name = "messagge")
@Table(name = "messagge")
public class Messagge {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "origin", referencedColumnName = "email")
	private User userOrig;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "destination", referencedColumnName = "email")
	private User userDest;

	private String subject;
	private String messagge;

	/* Getters y Setters */

	public User getUserOrig() {
		return userOrig;
	}

	public void setUserOrig(User userOrig) {
		this.userOrig = userOrig;
	}

	public User getUserDest() {
		return userDest;
	}

	public void setUserDest(User userDest) {
		this.userDest = userDest;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessagge() {
		return messagge;
	}

	public void setMessagge(String messagge) {
		this.messagge = messagge;
	}

	
	
}
