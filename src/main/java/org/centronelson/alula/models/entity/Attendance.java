package org.centronelson.alula.models.entity;

import java.util.Hashtable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity(name="Attendance")
@Table(name="attendance")
public class Attendance {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private  Long  userId;
	private String dateAtt;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getDateAtt() {
		return dateAtt;
	}
	public void setDateAtt(String dateAtt) {
		this.dateAtt = dateAtt;
	}
	@Override
	public String toString() {
		return "Attendance [id=" + id + ", userId=" + userId + ", dateAtt=" + dateAtt + "]";
	}
	

	
	
	
}
