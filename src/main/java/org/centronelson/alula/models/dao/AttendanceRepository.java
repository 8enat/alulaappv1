package org.centronelson.alula.models.dao;

import org.centronelson.alula.models.entity.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository("attendanceRepository")
public interface AttendanceRepository extends JpaRepository<Attendance, Long>{

}
