package org.centronelson.alula.models.dao;

import java.util.ArrayList;
import java.util.List;

import org.centronelson.alula.models.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {
	 User findByUsername(String username);
	 User findByEmail(String email);
	 List<User> findAllByOrderBySurname1AscSurname2AscNameAsc();
	 ArrayList<User> findByTasksId(Long id);
}