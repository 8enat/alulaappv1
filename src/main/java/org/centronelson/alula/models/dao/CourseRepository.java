package org.centronelson.alula.models.dao;

import java.util.List;
import org.centronelson.alula.models.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("courseRepository")
public interface CourseRepository extends JpaRepository<Course, Long> {
	Course findByName(String name);
	Course findBySubjectsId(Long id);
	List<Course> findAllByOrderByNameAsc();
}
