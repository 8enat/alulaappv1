package org.centronelson.alula.models.dao;

import java.util.ArrayList;

import org.centronelson.alula.models.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("taskRepository")
public interface TaskRepository extends JpaRepository<Task, Long>{
	ArrayList<Task> findBySubjectId(Long id);
	
}