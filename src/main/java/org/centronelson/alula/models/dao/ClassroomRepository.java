package org.centronelson.alula.models.dao;

import java.util.List;

import org.centronelson.alula.models.entity.Classroom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("classroomRepository")
public interface ClassroomRepository extends JpaRepository<Classroom, Long>{
	
	Classroom findByName(String name);
	List<Classroom> findAllByOrderByNameAsc();
}
