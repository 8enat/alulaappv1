package org.centronelson.alula.models.dao;


import java.util.List;
import org.centronelson.alula.models.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("subjectRepository")
public interface SubjectRepository extends JpaRepository<Subject, Long> {
	
	Subject findByName(String name);
	Subject findBySchedule(String schedule);
	List<Subject> findAllByOrderByNameAsc();
	List<Subject> findAllByOrderByIdAsc();
}
