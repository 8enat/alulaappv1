package org.centronelson.alula.models.dao;

import java.util.ArrayList;
import org.centronelson.alula.models.entity.Messagge;
import org.centronelson.alula.models.entity.User;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository("messaggeRepository")
public interface MessaggeRepository extends JpaRepository<Messagge, Long> {
	ArrayList<Messagge> findByUserOrigAndUserDest(User origin, User destination);
}
